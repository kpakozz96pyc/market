﻿using System;
using System.Net.Mail;
using System.Configuration;

namespace Market.Mailer
{
	public static class Mail
	{
		public static bool SendEmail(string subject, string content, string[] recipients, string from = null)
		{
			var emailLogin = ConfigurationManager.AppSettings["EmailLogin"];
			var emailPassword = ConfigurationManager.AppSettings["EmailPassword"];
			var emailFrom = String.IsNullOrEmpty(from) ? ConfigurationManager.AppSettings["EmailFrom"] : from;

			bool success = recipients != null && recipients.Length > 0;

			if (success)
			{
				var gmailClient = new SmtpClient
				{
					Host = ConfigurationManager.AppSettings["EmailHost"],
					Port = Int32.Parse(ConfigurationManager.AppSettings["EmailPort"]),
					EnableSsl = true,
					UseDefaultCredentials = false,
					Credentials = new System.Net.NetworkCredential(emailLogin, emailPassword)
				};


				using (var gMessage = new MailMessage("\"" + emailFrom + "\"" + "<" + emailLogin + ">", recipients[0], subject, content))
				{
					gMessage.IsBodyHtml = true;
					for (var i = 1; i < recipients.Length; i++)
						gMessage.To.Add(recipients[i]);

					try
					{
						gmailClient.Send(gMessage);
						success = true;
					}
					catch (Exception) { success = false; }
				}
			}
			return success;
		}
	}
}
