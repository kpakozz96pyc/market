﻿using System;
using System.Web;

namespace Market.Common
{
    public static class GlobalContextKeeper<T>
        where T : class
    {
        [ThreadStatic]
        private static T _currentThreadInfo;

        public static T Current
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    return (T)HttpContext.Current.Items[typeof(T)];
                }

                return _currentThreadInfo;
            }
            set
            {
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Items[typeof(T)] = value;
                }

                else
                {
                    _currentThreadInfo = value;
                }
            }
        }
    }
}