﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using NLog;

namespace Market.Common.Logging
{
    internal sealed class NLogger : ILog
    {
        private const string Source = "MarketLogger";
        private readonly Logger _logger = LogManager.GetLogger("NLogger");
        private static long _eventCount;

        public void Debug(string message, params KeyValuePair<string, object>[] additionalInfo)
        {
            Log(LogLevel.Debug, message, null, additionalInfo);
        }

        public void Info(string message)
        {
            Log(LogLevel.Info, message, null, null);
        }

        public void Warning(string message)
        {
            Log(LogLevel.Warn, message, null, null);
        }

        public void Error(string message)
        {
            Log(LogLevel.Error, message, null, null);
        }

        public void Error(Exception exception)
        {
            Log(LogLevel.Error, string.Empty, exception, null);
        }

        public void Error(string message, Exception exception)
        {
            Log(LogLevel.Error, message, exception, null);
        }

        private static void AddFileSplitterToLogMessage(StringBuilder message, string title)
        {
            message.Insert(0,
                ("==================== Start Event " + (title == null ? " " : "(" + title + ") ")).PadRight(80, '=') +
                Environment.NewLine);
            message.AppendLine()
                .Append("==================== End Event ")
                .AppendLine((title == null ? " " : "(" + title + ") ").PadRight(49, '='));
            message.AppendLine();
        }

        private static string GetExceptionHash(Exception e)
        {
            var strForHash = string.Empty;
            GetExceptionStackTrace(e, ref strForHash);
            var hash = BitConverter.GetBytes(strForHash.GetHashCode());
            return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
        }

        private static void GetExceptionStackTrace(Exception e, ref string stackTrace)
        {
            while (true)
            {
                stackTrace += e.StackTrace;
                if (e.InnerException != null)
                {
                    e = e.InnerException;
                    continue;
                }
                break;
            }
        }

        private static string GetLogException(Exception exception, IDictionary<object, object> properties)
        {
            var sb = new StringBuilder(Environment.NewLine);
            sb.AppendLine("------- Start Exception --------");
            LogException("   ", sb, exception, properties);
            return sb.ToString();
        }

        private static void LogException(string padding, StringBuilder sb, Exception exception, IDictionary<object, object> properties)
        {
            while (true)
            {
                var hash = GetExceptionHash(exception);
                var exceptionType = exception.GetType();

                sb.AppendLine(padding + "Exception Hash: " + hash);
                if (!properties.ContainsKey("ExceptionHash"))
                    properties.Add("ExceptionHash", hash);

                sb.AppendLine(padding + "Exception Type: " + exceptionType);
                if (!properties.ContainsKey("ExceptionType"))
                    properties.Add("ExceptionType", exceptionType);

                sb.AppendLine(padding + "Exception Source: " + exception.Source);
                if (!properties.ContainsKey("ExceptionSource"))
                    properties.Add("ExceptionSource", exception.Source);

                sb.AppendLine(padding + "Exception Message: " + exception.Message);
                if (!properties.ContainsKey("ExceptionMessage"))
                    properties.Add("ExceptionMessage", exception.Message);

                sb.AppendLine(padding + "Exception StackTrace: " + exception.StackTrace);
                if (!properties.ContainsKey("ExceptionStackTrace"))
                    properties.Add("ExceptionStackTrace", exception.StackTrace);

                var exceptionData = string.Empty;
                foreach (DictionaryEntry data in exception.Data)
                {
                    sb.AppendLine(string.Format("{0} {1}:{2}", padding, data.Key, data.Value));
                    exceptionData += string.Format("{0} {1}:{2}", padding, data.Key, data.Value);
                }
                if (!properties.ContainsKey("ExceptionData"))
                    properties.Add("ExceptionData", exceptionData);

                if (exception.InnerException != null)
                {
                    padding += "    ";
                    sb.AppendLine(padding + "InnerException: ");
                    exception = exception.InnerException;
                    continue;
                }
                break;
            }
        }

        private void Log(LogLevel logLevel, string userMessage, Exception exception, IEnumerable<KeyValuePair<string, object>> additionalInfo)
        {
            try
            {
                _eventCount += 1;
                var logEvent = GetLogEvent(userMessage, logLevel, exception, additionalInfo);
                _logger.Log(logEvent);
            }
            catch (Exception le)
            {
                // если возникла ошибка в логгере - залогируем ее в журнал событий
                var eventLog = new EventLog { Source = Source };
                var errorText = "Error write exception to log." + Environment.NewLine + GetLogException(le, new Dictionary<object, object>());
                eventLog.WriteEntry(errorText, EventLogEntryType.Error);

                eventLog = new EventLog { Source = Source };
                errorText = "error." + Environment.NewLine + userMessage;
                eventLog.WriteEntry(errorText, EventLogEntryType.Error);
            }
        }


        private static LogEventInfo GetLogEvent(string userMessage, LogLevel type, Exception exception, IEnumerable<KeyValuePair<string, object>> additionalInfo)
        {
            var properties = new Dictionary<object, object>();
            var message = new StringBuilder(256 + userMessage.Length);
            var threadId = GetThreadGuid();
            var date = DateTime.Now;

            message.Append("ThreadId: " + threadId + ": ").AppendLine()
                .Append("Date: ").Append(date.ToString("O")).AppendLine()
                .Append("Message: ").Append(userMessage).AppendLine();

            properties.Add("ThreadId", threadId);
            properties.Add("EventDate", date.ToString("u"));
            properties.Add("LogLevel", type.Name);

            if (exception != null)
            {
                message.AppendLine().Append(GetLogException(exception, properties));
            }

            if (additionalInfo != null)
            {
                foreach (var info in additionalInfo.Where(info => !properties.ContainsKey(info.Key)))
                {
                    properties.Add(info.Key, info.Value);
                }
            }

            AddFileSplitterToLogMessage(message, _eventCount.ToString(CultureInfo.InvariantCulture));
            var resultMessage = message.ToString();
            properties.Add("Message", userMessage);

            var result = new LogEventInfo(type, string.Empty, resultMessage);
            foreach (var property in properties)
            {
                result.Properties.Add(property);
            }

            return result;
        }

        private static string GetThreadGuid()
        {
            var current = GlobalContextKeeper<LocalGuid>.Current;
            if (current != null) 
                return current.Guid.ToString("N");

            current = new LocalGuid();
            GlobalContextKeeper<LocalGuid>.Current = current;

            return current.Guid.ToString("N");
        }

        private class LocalGuid
        {
            public LocalGuid()
            {
                Guid = Guid.NewGuid();
            }

            public Guid Guid { get; private set; }
        }
    }
}
