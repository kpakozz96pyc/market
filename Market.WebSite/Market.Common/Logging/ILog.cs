﻿using System;
using System.Collections.Generic;

namespace Market.Common.Logging
{
    public interface ILog
    {
        void Debug(string message, params KeyValuePair<string, object>[] additionalInfo);
        void Info(string message);
        void Warning(string message);
        void Error(string message);
        void Error(Exception exception);
        void Error(string message, Exception exception);
    }
}
