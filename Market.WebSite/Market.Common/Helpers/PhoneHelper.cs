﻿using System.Text.RegularExpressions;

namespace Market.Common.Helpers
{
    public static class PhoneHelper
    {
        public static string TransformPhone(string phoneInfo)
        {
            var s = Regex.Replace(phoneInfo, @"[\D+]", "");
            s = Regex.Replace(s, @"^8([0-9]{10})", "7$1");
            s = Regex.Replace(s, @"^9([0-9]{9})$", @"7$&");
            s = Regex.Replace(s, @"^9([0-9]{9})$", @"7$&");
            s = Regex.Replace(s, @"[-() ]", string.Empty);
            return s;
        }

        public static bool PhoneIsValid(string phoneNumber)
        {
            return Regex.IsMatch(phoneNumber, "^[7]{1}[0-9]{10}$");
        }
    }

}
