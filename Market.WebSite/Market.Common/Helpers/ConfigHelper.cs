﻿using System.Configuration;

namespace Market.Common.Helpers
{
    public static class ConfigHelper
    {
        /// <summary>
        /// Вспомогательный метод для того чтобы брать значения из Web.config либо дефолтное
        /// </summary>
        /// <param name="appParamName">Имя параметра в секции AppSettings</param>     
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        private static string GetParamValue(string appParamName, string defaultValue)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[appParamName])
                ? ConfigurationManager.AppSettings[appParamName]
                : defaultValue;
        }

        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MarketContext"].ConnectionString; }
        }

        public static string LogsPath
        {
            get { return GetParamValue("LogsPath", ""); }
        }

        public static string CryptKey { get { return GetParamValue("CryptKey", "1qazwsxedc1"); } }
    }
}
