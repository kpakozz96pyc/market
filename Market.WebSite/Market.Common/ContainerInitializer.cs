﻿using Market.Common.Logging;
using Ninject;

namespace Market.Common
{
    public static class ContainerInitializer
    {
        public static void Initialize(IKernel kernel)
        {
            kernel.Bind<ILog>().To<NLogger>();
        }
    }
}