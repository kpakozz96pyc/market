﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Market.Common
{
	public class MailRenderer
	{
		public static string RenderPartialViewToString(Controller controller, string viewName, object model)
		{
			controller.ViewData.Model = model;
			ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

			using (var stringWriter = new StringWriter())
			{
				var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData,
												  controller.TempData, stringWriter);
				viewResult.View.Render(viewContext, stringWriter);
				viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

				return stringWriter.GetStringBuilder().ToString();
			}
		}
	}
}
