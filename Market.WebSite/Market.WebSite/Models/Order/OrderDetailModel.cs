﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Market.WebSite.Models.Order
{
    public class OrderDetailModel
    {
	    public int OrderId { get; set; }

        [Display(Name = "Номер заказа")]
        public int OrderNumber { get { return OrderId + 1015; } }

	    [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [Display(Name = "Количество рыб")]
        public int Count { get; set; }

        [Display(Name = "Комментарий к заказу")]
        public string Comment { get; set; }

        public Guid AlternativeId { get; set; }
    }
}