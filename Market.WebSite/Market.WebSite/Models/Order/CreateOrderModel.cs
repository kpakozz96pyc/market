﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Market.WebSite.Models.Order
{
    public class CreateOrderModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Имя")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        [StringLength(50)]
        public string Patronymic { get; set; }

        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "*")]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

		[Display(Name = "Email")]
		[StringLength(255)]
		public string Email { get; set; }

		[Display(Name = "Город")]
		[StringLength(255)]
		public string Address { get; set; }

        [Required]
        [HiddenInput(DisplayValue = false)]
        [Range(1, 100)]
        public int Count { get; set; }

        [Display(Name = "Комментарий к заказу")]
        [StringLength(2500)]
        public string Comment { get; set; }
    }
}