﻿$(document).ready(function() {
	updateUI();
	if ($('#clearCart').val() > 0)
		clearCart();
	$("#PhoneNumber").mask("+7 (999)-999-99-99");
	setActiveTab();

    $('.blog-post-button').click(function() {
    	$(this).parent().find('.blog-post-inner').stop().slideToggle();
	    if ($(this).html() == "Читать дальше") {
		    $(this).html("Свернуть");
	    } else {
	    	$(this).html("Читать дальше");
	    }
    });

	//footer прилепили к низу страницы
    $('#content').css('min-height', $(window).height() - $('#header').height() - $('#footer').height());
	$(window).resize(function() {
		$('#content').css('min-height', $(window).height() - $('#header').height() - $('#footer').height());
	});

	$('#create-button').click(function () {
		if ($('#order-form').valid()) {
			$('#create-button').prop('disabled', true);
			$('#order-form').submit();
		} else {
			$('#create-button').prop('disabled', false);
		}
	});
});
	
function addToCart() {
	var cart = JSON.parse(localStorage.getItem('cart'));
	if (cart) {
		cart.items += 1;
	} else {
		cart = { items: 1 };
	}
	localStorage.setItem('cart', JSON.stringify(cart));
	updateUI();
}

function deleteFromCart() {
	var cart = JSON.parse(localStorage.getItem('cart'));
	if (cart) {
		if (cart.items > 0)
			cart.items -= 1;
	} else {
		cart = { items: 0 };
	}
	localStorage.setItem('cart', JSON.stringify(cart));
	updateUI();
}

function clearCart() {
	var cart = { items: 0 };
	localStorage.setItem('cart', JSON.stringify(cart));
	updateUI();
}
function updateUI() {
	var cart = JSON.parse(localStorage.getItem('cart'));
	var cartElement = $('#cartElement');
	var cartCount = $('#totalCount');
	var totalPrice = $('#totalPrice');
	var hiddenCount = $('#Count');
	var orderButtons = $('#orderButtons');

	if (cartElement) {
		if (cart) {
			cartElement.html(cart.items + " шт.");
			cartCount.val(cart.items);
			totalPrice.html(cart.items * 850 + " р.");
			hiddenCount.val(cart.items);
			if (cart.items > 0)
				orderButtons.show();
			else
				orderButtons.hide();
		} else {
			cartElement.html(0 + " шт.");
			cartCount.val(0);
			totalPrice.html(0 + " р.");
			hiddenCount.val(0);
		}
	}
}

function setActiveTab() {
	var tabclass = $('#activetab').val();
	if (tabclass) {
		$("."+tabclass).addClass('active');
	}
}

