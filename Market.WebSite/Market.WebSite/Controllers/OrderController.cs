﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Market.Data.Context;
using Market.WebSite.Models.Order;

namespace Market.WebSite.Controllers
{
    public class OrderController : BaseController
    {

        [HttpGet]
        public async Task<ActionResult> Index(Guid id)
        {
            Order order;
            using (var context = new MarketContext())
                order = await context.Orders.Where(o => o.AlternativeId == id).SingleOrDefaultAsync();

            if (order == null) throw new ArgumentException("Ошибка");

            var model = new OrderDetailModel
            {
                OrderId = order.Id,
                Count = order.Count,
                Address = order.Address,
                FirstName = order.FirstName,
                LastName = order.LastName,
                Patronymic = order.Patronymic,
                PhoneNumber = order.PhoneNumber,
                Comment = order.Comment,
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOrderModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Заполните все поля";
                return View();
            }

            var now = DateTime.UtcNow;
            var order = new Order
                {
                    Address = model.Address,
                    CreationDate = now,
                    ModificationDate = now,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    Patronymic = model.Patronymic,
                    Count = model.Count,
                    Comment = model.Comment
                };

            using (var context = new MarketContext())
            {
                context.Orders.Add(order);
                await context.SaveChangesAsync();
            }

            var createdOrderModel = new OrderDetailModel
            {
                OrderId = order.Id,
                Count = order.Count,
                Address = order.Address,
                FirstName = order.FirstName,
                LastName = order.LastName,
                Patronymic = order.Patronymic,
                PhoneNumber = order.PhoneNumber,
                Comment = order.Comment,
                AlternativeId = order.AlternativeId
            };

            Market.Mailer.Mail.SendEmail("Заказ" + createdOrderModel.OrderNumber,
                Market.Common.MailRenderer.RenderPartialViewToString(this, "MailBody", createdOrderModel),
                new[] { ConfigurationManager.AppSettings["EmailLogin"], "Pahkin@bk.ru" });

            return RedirectToAction("Index", "Order", new { id = order.AlternativeId });
        }


        public ActionResult MailBody(OrderDetailModel model)
        {
            return View(model);
        }

    }
}