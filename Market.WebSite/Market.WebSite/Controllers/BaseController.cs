﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;

namespace Market.WebSite.Controllers
{
    //[Context]
    //[Logging]
    public class BaseController : Controller
    {
        public void SetError<T>(Expression<Func<T>> memberExpression, string message)
        {
            var expressionBody = (MemberExpression)memberExpression.Body;
            ModelState.AddModelError(expressionBody.Member.Name, message);
        }

        public void SetError(string message)
        {
            ModelState.AddModelError("", message);
        }
    }

}