﻿using System.Web;
using System.Web.Mvc;
using Market.WebSite.Common;

namespace Market.WebSite
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
