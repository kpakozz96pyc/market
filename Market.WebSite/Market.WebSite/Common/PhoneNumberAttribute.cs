﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Market.Common.Helpers;
using EventSource = System.Diagnostics.Tracing.EventSource;

namespace Market.WebSite.Common
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PhoneNumberAttribute : RequiredAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var str = Convert.ToString(value, CultureInfo.CurrentCulture);
            if (string.IsNullOrEmpty(str))
            {
                return new ValidationResult("Не задан номер телефона");
            }
            var phone = PhoneHelper.TransformPhone(str);

            return PhoneHelper.PhoneIsValid(phone) ? ValidationResult.Success : new ValidationResult("Неправильно задан номер телефона");
        }
    }
}