﻿using System;
using System.Web;
using System.Web.Mvc;
using Market.Common.Logging;

namespace Market.WebSite.Common
{
    public class ExceptionFilter : IExceptionFilter
    {
        private readonly ILog _log;
        public ExceptionFilter(ILog log)
        {
            _log = log;
        }

        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            var actionName = filterContext.RouteData.Values["action"].ToString();
            var controllerName = filterContext.RouteData.Values["controller"].ToString();

            var additionalInfo = string.Format("Controller = {0}. Action = {1}", controllerName, actionName);
            _log.Error(additionalInfo, exception);

            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary<HandleErrorInfo>(new HandleErrorInfo(exception, controllerName, actionName)),
                TempData = filterContext.Controller.TempData
            };

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = new HttpException(null, exception).GetHttpCode();
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}