﻿namespace Market.Data.Context
{
    public enum OrderState
    {
        New = 0,
        Processed = 1
    }
}