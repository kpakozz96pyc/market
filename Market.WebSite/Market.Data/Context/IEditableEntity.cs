﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Market.Data.Context
{
    public interface IEditableEntity
    {
        [Required]
        [Column(TypeName = "datetime2")]
        DateTime ModificationDate { get; set; }
    }
}