﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Data.Context
{
    public class Order : BaseEntity, IEditableEntity, ICreatableEntity
    {
        [Required]
        [MaxLength(250)]
        public string FirstName { get; set; }
        
        [MaxLength(250)]
        public string LastName { get; set; }
        
        [MaxLength(250)]
        public string Patronymic { get; set; }

        [Required]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }
        
        [MaxLength(250)]
        public string Address { get; set; }

        [Range(1, 100)]
        public int Count { get; set; }

        public DateTime ModificationDate { get; set; }
        public DateTime CreationDate { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AlternativeId { get; set; }

        [MaxLength(2500)]
        public string Comment { get; set; }
    }
}
