using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace Market.Data.Context
{
    public class MarketContext : DbContext
    {
        public MarketContext() : base("MarketContext") { }

        public DbSet<Order> Orders { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Order>()
                .Property(f => f.ModificationDate)
                .HasColumnType("datetime2");

            modelBuilder.Entity<Order>()
                .Property(f => f.CreationDate)
                .HasColumnType("datetime2");
        }
    }
}