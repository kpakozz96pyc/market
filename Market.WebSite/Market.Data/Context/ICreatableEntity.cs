﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Market.Data.Context
{
    public interface ICreatableEntity
    {
        [Required]
        [Column(TypeName = "datetime2")]
        DateTime CreationDate { get; set; }
    }
}
